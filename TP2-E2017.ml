(*******************************************************************)
(* Langages de Programmation: IFT 3000 NRC 51158                   *)
(* TP2 �T� 2017. Date limite: Jeudi 13 juillet � 17h00             *)
(* Enseignant: Mondher Bouden (ift3000-e2017@ift.ulaval.ca)        *)
(* Implanter un syst�me expert en utilisant l'orient� objet        *)
(*******************************************************************)
(*                                                                 *)
(* NOM: Brochu PR�NOM:Louis-Mathieu *)
(* MATRICULE: 111076019 PROGRAMME: ift *)
(*                                                                 *)
(*******************************************************************)

(* Chargement de fonctions et librairies utiles pour le TP2 *)
#use "utiles.ml";;

(* Charger la signature du TP2 *)
#use "TP2-SIG-E2017.mli";;

module Tp2e17 : TP2E17 =
  struct

    type fait = string

    class regle (p: fait list) (c: fait list) (u: bool) =
      object
	val premisses : fait list = p
	val conclusions : fait list = c
	val mutable utilisee : bool = u
	method get_premisses = premisses
	method get_conclusions = conclusions
	method get_utilisee = utilisee
	method set_utilisee (vu : bool) = utilisee <- vu
      end

    class sexpert (ss:string) =
      object
	val sorte_sexpert : string = ss
	method get_sorte_sexpert = sorte_sexpert
      end

    class sexpert_taxonomique (ss:string) (ns:string) =
      object(self)
	inherit sexpert ss as parent
	val nom_sexpert : string = ns
	val mutable base_regles : regle list = []
	val mutable base_faits : fait list = []
	method get_nom_sexpert = nom_sexpert
	method get_base_regles = base_regles
	method get_base_faits = base_faits
	method set_base_regles (lr : regle list) = base_regles <- lr
	method set_base_faits (lf : fait list) = base_faits <- lf
	initializer print_string ("Nouveau " ^ (parent#get_sorte_sexpert) ^ ": " ^
				     (self#get_nom_sexpert)) ; print_newline()

	(* M�thodes � implanter *)

	(* regle_existe : regle -> bool  *)
	method regle_existe (r:regle) = 
		appartient r base_regles

	(* fait_existe : fait -> bool  *)
	method fait_existe (f:fait) = 
		appartient f base_faits

	(* ajouter_regle : regle -> unit *)
	method ajouter_regle (r:regle) = 
		if appartient r base_regles then () else base_regles <- base_regles @ [r]

  (* supprimer_regle : regle -> unit  *)
	method supprimer_regle (r:regle) = 
		if base_regles = [] && base_faits = [] then
			failwith "Le systeme expert est vide";
			
		if appartient r base_regles then
			base_regles <- enlever r base_regles
		else
			failwith "Le systeme expert ne contient pas cette regle"

	(* ajouter_fait : fait -> unit *)
	method ajouter_fait (f:fait) =
		if appartient f base_faits then () else base_faits <- base_faits @ [f]

        (* supprimer_fait : fait -> unit  *)
	method supprimer_fait (f:fait) = 
		if base_regles = [] && base_faits = [] then
			failwith "Le systeme expert est vide";
		if appartient f base_faits then
			base_faits <- enlever f base_faits
		else
				failwith "Le systeme expert ne contient pas ce fait"

	(* vider_base_regles : unit *)
	method vider_base_regles = 
		base_regles <- []

	(* vider_base_faits : unit *)
	method vider_base_faits =
		base_faits <- []

	(* intialiser_utilisee_regles : unit *)
	method initialiser_utilisee_regles =
		iter (fun r -> r#set_utilisee false) base_regles

	(* ajouter_liste_regles : (fait list * fait list) list -> unit *)
	method ajouter_liste_regles (lr:(fait list * fait list) list) =
		iter 
		(fun pr -> 
			let r = new regle (fst pr) (snd pr) false in
			self#ajouter_regle r
		)
		 lr

	(* ajouter_liste_faits : fait list -> unit *)
	method ajouter_liste_faits (lf:fait list) =
	iter (fun f -> self#ajouter_fait f) lf

  (* charger_donnees :string ->  unit *)
  method charger_donnees (fichier:string) =
		try
			let flux = open_in fichier in 
			let paire = lire_fichier flux ";" in
			self#vider_base_regles;
			self#vider_base_faits;(*Exception safe pcq l'erreur survient a la 1ere ligne*)
			self#ajouter_liste_regles (fst paire);
			self#ajouter_liste_faits (snd paire);
			close_in flux;
		with
		| _ -> print_string "Fichier inacessible"

  (* afficher_sexpert : unit *)
	method afficher_sexpert =
		if base_faits = [] && base_regles = [] 
		then print_string "Le systeme expert est vide"
		else
		(
		print_string "Base des regles:\n";
		if base_regles = [] then print_string "Liste vide\n"
		else
		(
  		iteri
  		(fun i r ->
  			let premisses = r#get_premisses and conclusions = r#get_conclusions
  			and utilisee = r#get_utilisee in 
  			print_string ("Regle numero " ^ (string_of_int (i + 1)) ^":\n");
  			print_string "Premisses: ";
  			print_string (hd premisses);
  			iter (fun prem -> print_string ("; " ^ prem)) (tl premisses);
  			print_string "\n";
  			print_string "Conclusions: ";
  			print_string (hd conclusions);
  			iter (fun concl -> print_string ("; " ^ concl)) (tl conclusions);
  			print_string "\n";
  			print_string ("utilisee = " ^ (string_of_bool utilisee));
  			print_string "\n";
  		)
  		base_regles;
		);
		print_string "Base des faits:\n";

		if base_faits = [] then print_string "Liste vide\n"
		else
			( 
			print_string (hd base_faits);
			iter (fun fait -> print_string ("; " ^ fait)) (tl base_faits);
			)
		)

  (* chainage_avant : int *)
	method chainage_avant =
		if base_regles = [] && base_faits = [] then
			failwith "Le systeme expert est vide";
		if base_regles = [] then 
			failwith "Le systeme expert ne contient pas de regles";
		if base_faits = [] then 
			failwith "Le systeme expert ne contient pas de faits";
		let n = ref 0 and b = ref false in 
		let rec rec_chainage_avant() = 
		iter
		(
			fun r -> 
				if ((r#get_utilisee = false) && 
					(for_all (fun prem -> self#fait_existe prem) r#get_premisses)) then
					begin
					iter 
					(
						fun conc -> if (not (self#fait_existe conc)) then
							begin 
							self#ajouter_fait conc;
							r#set_utilisee true;
							n := !n + 1;
							b := true;
							
							end
					) 
					r#get_conclusions;
					
					end 
		)
		base_regles;
		if !b = false then
			!n
		else
			begin
				b := false;
				rec_chainage_avant();
			end
		in rec_chainage_avant()
		
		

  (* chainage_arriere : fait -> bool *)
	method chainage_arriere (f:fait) =
		if base_regles = [] && base_faits = [] then
			failwith "Le systeme expert est vide";
		if base_regles = [] then 
			failwith "Le systeme expert ne contient pas de regles";
		if base_faits = [] then 
			failwith "Le systeme expert ne contient pas de faits"; 
	let rec rec_chainage_arriere f (f_args: fait list) =
		match base_regles,base_faits with
		| [],[] -> failwith "Le systeme expert est vide"
		| _,[] -> failwith "Le systeme expert ne contient pas de faits"
		| [],_ -> failwith "Le systeme expert ne contient pas de regles"
		| br,bf ->
			if (appartient f bf) then true
			else
				exists
				(
					fun r ->
						(for_all (fun pr -> not (appartient pr f_args)) r#get_premisses) 
						&&
						(appartient f r#get_conclusions) 
						&& 
						(for_all (fun pr -> (appartient pr bf) || (rec_chainage_arriere pr (pr::f_args))) r#get_premisses)
				)
				br
	in rec_chainage_arriere f [f]
      end

  end

