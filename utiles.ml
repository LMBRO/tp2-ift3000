(*********************************************************************)
(* Langages de Programmation: IFT 3000 NRC 51158                     *)
(* Fonctions et librairies utiles pour le TP2                        *) 
(* Implanter un syst�me expert en utilisant l'orient� objet          *)
(*********************************************************************)

#load "str.cma";;  (* Charger le module Str  *)

open List
open Str 

(******************************************************************)
(* Fonctions fournies (vous pouvez en ajouter au besoin ...)      *)
(* ****************************************************************)

(* appartient : 'a -> 'a list -> bool                   *)
(* Retourner si un �l�ment existe ou non dans une liste *)

let appartient e l = exists (fun x -> x = e) l

(* enlever : 'a -> 'a list -> 'a list *)
(* Enlever un �l�ment dans une liste  *)

let enlever e l = 
  let (l1, l2) = partition (fun x -> x = e) l
  in l2

(* remplacer : 'a -> 'a -> 'a list -> 'a list       *)
(* Remplacer un �l�ment par un autre dans une liste *)

let remplacer e e' l =
  map (fun x -> (if (x = e) then e' else x)) l 

(* decouper_chaine : string -> string -> string list                          *)
(* Retourner une liste en d�coupant une cha�ne selon un s�parateur (p.ex ";") *)

let decouper_chaine chaine separateur = split (regexp separateur) chaine

(* read_line : in_channel -> string                        *)
(* Permet de lire une ligne dans un fichier                *)
(* Elle retourne une cha�ne vide si le fichier est termin� *)
 
let lire_ligne ic =
   try
     input_line ic (* Lire une ligne *)
   with End_of_file -> "" 

(* lire_fichier : in_channel -> string -> (string list * string list) list * string list *)
(* Lire un fichier CSV et retourne une paire dont le premier �l�ment *)
(* repr�sente la liste des r�gles (liste de pr�misses et de *)
(* conclusions) et le deuxi�me �l�memnt repr�sente la liste des r�gles *)
(* en sp�cifiant le s�parateur qu'il faut utiliser pour d�limiter les cha�nes  *)

let lire_fichier (flux:in_channel) (separateur:string) =
  let rec lire_regles f s =
    let ligne1 = lire_ligne flux in (* lire les pr�misses *)
    if (ligne1 = "!!") then [] else
    let ligne2 = lire_ligne flux in (* lire les conclusions *)
    match (ligne1,ligne2) with
    | ("","") -> []
    | ("",_) -> []
    | (_,"") -> []
    | (s1,s2) -> ((decouper_chaine (String.trim s1) separateur)
                ,(decouper_chaine (String.trim s2) separateur))
                 ::(lire_regles flux separateur) in
  let liste_regles = lire_regles flux separateur in
  let ligne = lire_ligne flux in (* lire la liste des faits *)
  let liste_faits = (decouper_chaine (String.trim ligne) separateur) in
  (liste_regles, liste_faits)
