(***************************************************************************)
(* Jeu d'essai - TP2 - �T� 2017                                            *)
(***************************************************************************)

(* Pour changer ou obtenir le r�pertoire courant
Sys.getcwd ();;
Sys.chdir;;
*)

(* Pour afficher les listes avec plus de �profondeurs�:
#print_depth 1000;;
#print_length 1000;;
*)

(* On charge le fichier ml du Tp apr�s avoir implant�
les m�thodes demand�es pour realiser les tests  *)

#use "TP2-E2017.ml";;

(* R�sultat:
val appartient : 'a -> 'a list -> bool = <fun>
val enlever : 'a -> 'a list -> 'a list = <fun>
val remplacer : 'a -> 'a -> 'a list -> 'a list = <fun>
val decouper_chaine : string -> string -> string list = <fun>
val lire_ligne : in_channel -> string = <fun>
val lire_fichier :
  in_channel -> string -> (string list * string list) list * string list =
  <fun>
module type TP2E17 =
  sig
    type fait = string
    class regle :
      fait list ->
      fait list ->
      bool ->
      object
        val conclusions : fait list
        val premisses : fait list
        val mutable utilisee : bool
        method get_conclusions : fait list
        method get_premisses : fait list
        method get_utilisee : bool
        method set_utilisee : bool -> unit
      end
    class sexpert :
      string ->
      object val sorte_sexpert : string method get_sorte_sexpert : string end
    class sexpert_taxonomique :
      string ->
      string ->
      object
        val mutable base_faits : fait list
        val mutable base_regles : regle list
        val nom_sexpert : string
        val sorte_sexpert : string
        method afficher_sexpert : unit
        method ajouter_fait : fait -> unit
        method ajouter_liste_faits : fait list -> unit
        method ajouter_liste_regles : (fait list * fait list) list -> unit
        method ajouter_regle : regle -> unit
        method chainage_arriere : fait -> bool
        method chainage_avant : int
        method charger_donnees : string -> unit
        method fait_existe : fait -> bool
        method get_base_faits : fait list
        method get_base_regles : regle list
        method get_nom_sexpert : string
        method get_sorte_sexpert : string
        method initialiser_utilisee_regles : unit
        method regle_existe : regle -> bool
        method set_base_faits : fait list -> unit
        method set_base_regles : regle list -> unit
        method supprimer_fait : fait -> unit
        method supprimer_regle : regle -> unit
        method vider_base_faits : unit
        method vider_base_regles : unit
      end
  end
module Tp2e17 : TP2E17
*)

(* On ouvre le module disposant de fonctions pertinentes pour nos tests *)
open Tp2e17;;

(* On ex�cute maintenant les m�thodes une � une *)

let se = new sexpert_taxonomique "Systeme expert taxonomique" "Classer des especes animales";;

(* R�sultat:
Nouveau Syst�me expert taxonomique: Classer des esp�ces animales
val se : Tp2e14.sexpert_taxonomique = <obj>
*)

let r1 = new regle ["a des poils"] ["est un mammifere"] false;;

(* R�sultat:
val r1 : Tp2e17.regle = <obj>
*)

se#ajouter_regle r1;;

(* R�sultat:
- : unit = ()
*)

se#afficher_sexpert;;

(* R�sultat:
Base des regles:
Regle numero 1:
Premisses: a des poils
Conclusions: est un mammifere
utilisee = false
Base des faits:
Liste vide
- : unit = ()
*)


se#ajouter_liste_regles
    [(["nourrit ses petits au lait"], ["est un mammifere"]);
     (["a des plumes"],["est un oiseau"]);
     (["vole";"pond des oeufs"], ["est un oiseau"]);
     (["est un mammifere";"mange de la viande"],["est un carnivore"]);
     (["est un mammifere";"a les yeux diriges en avant";"a des dents pointues";"a des griffes"],["est un carnivore"])];;

(* R�sultat:
- : unit = ()
*)

se#regle_existe r1;;

(* R�sultat:
- : bool = true
*)

se#ajouter_fait "a des poils";;

(* R�sultat:
- : unit = ()
*)

se#ajouter_liste_faits ["a des ongles";"a un long cou";"est brun";"a des taches noires"];;

(* R�sultat:
- : unit = ()
*)

se#afficher_sexpert;;

(* R�sultat:
Base des regles:
Regle numero 1:
Premisses: a des poils
Conclusions: est un mammifere
utilisee = false
Regle numero 2:
Premisses: nourrit ses petits au lait
Conclusions: est un mammifere
utilisee = false
Regle numero 3:
Premisses: a des plumes
Conclusions: est un oiseau
utilisee = false
Regle numero 4:
Premisses: vole; pond des oeufs
Conclusions: est un oiseau
utilisee = false
Regle numero 5:
Premisses: est un mammifere; mange de la viande
Conclusions: est un carnivore
utilisee = false
Regle numero 6:
Premisses: est un mammifere; a les yeux diriges en avant; a des dents pointues; a des griffes
Conclusions: est un carnivore
utilisee = false
Base des faits:
a des poils; a des ongles; a un long cou; est brun; a des taches noires
- : unit = ()
*)

se#supprimer_regle r1;;

(* R�sultat:
- : unit = ()
*)

se#supprimer_fait "a des poils";;

(* R�sultat:
- : unit = ()
 *)

se#vider_base_regles;;

(* R�sultat:
- : unit = ()
*)

se#vider_base_faits;;

(* R�sultat:
- : unit = ()
*)

se#afficher_sexpert;;

(* R�sultat:
Le systeme expert est vide
- : unit = ()
*)

se#charger_donnees "Animaux.txt";;

(* R�sultat:
- : unit = ()
*)

se#afficher_sexpert;;

(* R�sultat:
Base des regles:
Regle numero 1:
Premisses: a des poils
Conclusions: est un mammifere
utilisee = false
Regle numero 2:
Premisses: nourrit ses petits au lait
Conclusions: est un mammifere
utilisee = false
Regle numero 3:
Premisses: a des plumes
Conclusions: est un oiseau
utilisee = false
Regle numero 4:
Premisses: vole; pond des oeufs
Conclusions: est un oiseau
utilisee = false
Regle numero 5:
Premisses: est un mammifere; mange de la viande
Conclusions: est un carnivore
utilisee = false
Regle numero 6:
Premisses: est un mammifere; a les yeux diriges en avant; a des dents pointues; a des griffes
Conclusions: est un carnivore
utilisee = false
Regle numero 7:
Premisses: est un mammifere; a des ongles
Conclusions: est un ongule
utilisee = false
Regle numero 8:
Premisses: est un carnivore; est brun; a des taches noires
Conclusions: est un guepard
utilisee = false
Regle numero 9:
Premisses: est un carnivore; est brun; a des raies noires
Conclusions: est un tigre
utilisee = false
Regle numero 10:
Premisses: est un ongule; a un long cou; est brun; a des taches noires
Conclusions: est une girafe
utilisee = false
Regle numero 11:
Premisses: est un ongule; est blanc; a des raies noires
Conclusions: est un zebre
utilisee = false
Regle numero 12:
Premisses: est un oiseau; ne vole pas
Conclusions: est un oiseau non volant
utilisee = false
Regle numero 13:
Premisses: est un oiseau; nage
Conclusions: est un oiseau non volant
utilisee = false
Regle numero 14:
Premisses: est un oiseau non volant; a des pattes longues; a un long cou; est noir et blanc
Conclusions: est une autruche
utilisee = false
Regle numero 15:
Premisses: est un oiseau non volant; a des pattes palmees; est noir et blanc
Conclusions: est un pingouin
utilisee = false
Regle numero 16:
Premisses: est un oiseau; vole remarquablement
Conclusions: est un albatros
utilisee = false
Base des faits:
a des poils; a des dents pointues; a des griffes; a les yeux diriges en avant; est brun; a des taches noires
- : unit = ()
*)


se#chainage_arriere "est un guepard";;

(* R�sultat:
- : bool = true
*)

se#vider_base_faits;;

(* R�sultat:
- : unit = ()
*)

se#ajouter_liste_faits ["a des poils";"a des ongles";"a un long cou";"est brun";
                        "a des taches noires"];;

(* R�sultat:
- : unit = ()
*)

se#chainage_avant;;

(* R�sultat:
- : int = 3
*)

se#afficher_sexpert;;

(* R�sultat:
Base des regles:
Regle numero 1:
Premisses: a des poils
Conclusions: est un mammifere
utilisee = true
Regle numero 2:
Premisses: nourrit ses petits au lait
Conclusions: est un mammifere
utilisee = false
Regle numero 3:
Premisses: a des plumes
Conclusions: est un oiseau
utilisee = false
Regle numero 4:
Premisses: vole; pond des oeufs
Conclusions: est un oiseau
utilisee = false
Regle numero 5:
Premisses: est un mammifere; mange de la viande
Conclusions: est un carnivore
utilisee = false
Regle numero 6:
Premisses: est un mammifere; a les yeux diriges en avant; a des dents pointues; a des griffes
Conclusions: est un carnivore
utilisee = false
Regle numero 7:
Premisses: est un mammifere; a des ongles
Conclusions: est un ongule
utilisee = true
Regle numero 8:
Premisses: est un carnivore; est brun; a des taches noires
Conclusions: est un guepard
utilisee = false
Regle numero 9:
Premisses: est un carnivore; est brun; a des raies noires
Conclusions: est un tigre
utilisee = false
Regle numero 10:
Premisses: est un ongule; a un long cou; est brun; a des taches noires
Conclusions: est une girafe
utilisee = true
Regle numero 11:
Premisses: est un ongule; est blanc; a des raies noires
Conclusions: est un zebre
utilisee = false
Regle numero 12:
Premisses: est un oiseau; ne vole pas
Conclusions: est un oiseau non volant
utilisee = false
Regle numero 13:
Premisses: est un oiseau; nage
Conclusions: est un oiseau non volant
utilisee = false
Regle numero 14:
Premisses: est un oiseau non volant; a des pattes longues; a un long cou; est noir et blanc
Conclusions: est une autruche
utilisee = false
Regle numero 15:
Premisses: est un oiseau non volant; a des pattes palmees; est noir et blanc
Conclusions: est un pingouin
utilisee = false
Regle numero 16:
Premisses: est un oiseau; vole remarquablement
Conclusions: est un albatros
utilisee = false
Base des faits:
a des poils; a des ongles; a un long cou; est brun; a des taches noires; est un mammifere; est un ongule; est une girafe
- : unit = ()
*)

se#fait_existe "est une girafe";;

(* R�sultat:
- : bool = true
*)

se#initialiser_utilisee_regles;;

(* R�sultat:
- : unit = ()
*)

se#afficher_sexpert;;

(* R�sultat:
Base des regles:
Regle numero 1:
Premisses: a des poils
Conclusions: est un mammifere
utilisee = false
Regle numero 2:
Premisses: nourrit ses petits au lait
Conclusions: est un mammifere
utilisee = false
Regle numero 3:
Premisses: a des plumes
Conclusions: est un oiseau
utilisee = false
Regle numero 4:
Premisses: vole; pond des oeufs
Conclusions: est un oiseau
utilisee = false
Regle numero 5:
Premisses: est un mammifere; mange de la viande
Conclusions: est un carnivore
utilisee = false
Regle numero 6:
Premisses: est un mammifere; a les yeux diriges en avant; a des dents pointues; a des griffes
Conclusions: est un carnivore
utilisee = false
Regle numero 7:
Premisses: est un mammifere; a des ongles
Conclusions: est un ongule
utilisee = false
Regle numero 8:
Premisses: est un carnivore; est brun; a des taches noires
Conclusions: est un guepard
utilisee = false
Regle numero 9:
Premisses: est un carnivore; est brun; a des raies noires
Conclusions: est un tigre
utilisee = false
Regle numero 10:
Premisses: est un ongule; a un long cou; est brun; a des taches noires
Conclusions: est une girafe
utilisee = false
Regle numero 11:
Premisses: est un ongule; est blanc; a des raies noires
Conclusions: est un zebre
utilisee = false
Regle numero 12:
Premisses: est un oiseau; ne vole pas
Conclusions: est un oiseau non volant
utilisee = false
Regle numero 13:
Premisses: est un oiseau; nage
Conclusions: est un oiseau non volant
utilisee = false
Regle numero 14:
Premisses: est un oiseau non volant; a des pattes longues; a un long cou; est noir et blanc
Conclusions: est une autruche
utilisee = false
Regle numero 15:
Premisses: est un oiseau non volant; a des pattes palmees; est noir et blanc
Conclusions: est un pingouin
utilisee = false
Regle numero 16:
Premisses: est un oiseau; vole remarquablement
Conclusions: est un albatros
utilisee = false
Base des faits:
a des poils; a des ongles; a un long cou; est brun; a des taches noires; est un mammifere; est un ongule; est une girafe
- : unit = ()
*)

(***************************************)
(* Verification des essages d'erreurs *)
(***************************************)

try
  ignore ((new sexpert_taxonomique "" "")#supprimer_regle r1)
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Le systeme expert est vide
- : unit = ()
*)

try
  ignore (se#supprimer_regle r1)
with
  Failure s -> print_endline s;;

(* R�sultat:
Le systeme expert ne contient pas cette regle
- : unit = ()
*)

try
  ignore ((new sexpert_taxonomique "" "")#supprimer_fait "test")
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Le systeme expert est vide
- : unit = ()
*)

try
  ignore (se#supprimer_fait "fait existe pas")
with
  Failure s -> print_endline s;;

(* R�sultat:
Le systeme expert ne contient pas ce fait
- : unit = ()
*)

try
  ignore ((new sexpert_taxonomique "" "")#chainage_avant)
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Le systeme expert est vide
- : unit = ()
*)

try
  let s' = new sexpert_taxonomique "" "" in
  ignore (s'#ajouter_fait "a des poils");
  ignore (s'#chainage_avant)
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Le systeme expert ne contient pas de regles
- : unit = ()
*)

try
  let s' = new sexpert_taxonomique "" "" in
  ignore (s'#ajouter_regle r1);
  ignore (s'#chainage_avant)
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Le systeme expert ne contient pas de faits
- : unit = ()
*)

try
  let s' = new sexpert_taxonomique "" "" in
  ignore (s'#ajouter_fait "a des poils");
  ignore (s'#chainage_arriere "")
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Le systeme expert ne contient pas de regles
- : unit = ()
*)

try
  let s' = new sexpert_taxonomique "" "" in
  ignore (s'#ajouter_regle r1);
  ignore (s'#chainage_arriere "")
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Le systeme expert ne contient pas de faits
- : unit = ()
*)

try
  let s' = new sexpert_taxonomique "" "" in
  s'#charger_donnees "Fichier inexistant"
with
  Failure s -> print_endline s;;

(* R�sultat:
Nouveau :
Fichier inacessible
- : unit = ()
*)
